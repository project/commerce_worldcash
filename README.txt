Commerce WorldCash
==================

WorldCash integration for the Drupal Commerce payment and checkout system.
Currently supports immediate payments from Australian banks on the checkout
form. Use this module for people who do not have credit cards.

Sponsored by WorldCash Payment Systems Inc.

Installation and Configuration
------------------------------
* Install the WorldCash module.
* Enable the payment method rule titled WorldCash via
       Store > Configuration > Payment methods.
* Add your merchant API Key and ensure currency is set to AUD.

Registering with WorldCash
--------------------------
You must have a merchant account with WorldCash in order to use this module, as
it depends on API credentials specific to our services. If you need to register
for an account, go to https://www.worldcash.net. It is free.

If you just want to perform development tests using this module, you must
generate an API test key on your account.

Development notes
-----------------
Developer guides can be found at https://www.worldcash.net/developer.

Credits
-------
Developed by GO1 - www.go1.com.au
